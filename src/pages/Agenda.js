import CalendarioVisitas from "../components/CalendarioVisitas";


function Agenda(){
return(
    <div className="page container mt-3" id="about-page">
        <h1 className="mb-3">Calendário de Visitas</h1>
        <CalendarioVisitas/>
    </div>
)
}

export default Agenda;