import React from "react"
import formei from './../resources/img/publicacoes/formei.png'

function Sobre() {
  return (
    <div className="page container mt-3" id="about-page">

      <h1 className="mb-3">Sobre a Formei!</h1>
      <div className="row">
        <p>A Formei! é uma organização <span className="text-primary"><b>dedicada a incentivar, motivar e orientar estudantes do ensino fundamental e médio a prosseguirem seus estudos</b></span> e buscarem uma educação de nível superior ou uma formação tecnológica.
        </p>
        <p>
          A ideia do projeto é incentivar jovens, principalmente de baixa renda, a seguir nos estudos passando informações sobre cursos superiores, formas de ingresso, entre outros. <br />Tudo isso através de publicações nas redes sociais, conteúdos maiores no website (blog) e visitas a escolas.
        </p>
        <p>
          Nosso principal objetivo é combater a evasão escolar e promover a conscientização sobre a importância da educação continuada. <br />
          Acreditamos que a educação é um pilar fundamental para o desenvolvimento pessoal e profissional dos jovens, capacitando-os para enfrentar os desafios futuros.
        </p>
        {/* <p>
          Para alcançar esses objetivos, a Formei! oferece uma variedade de programas e recursos educacionais. <br/>
          Desenvolvemos atividades extracurriculares, como workshops, palestras e feiras educacionais, que proporcionam aos estudantes a oportunidade de explorar diferentes áreas de conhecimento e carreiras profissionais. <br/>
          Essas atividades visam despertar o interesse dos alunos, ampliar suas perspectivas e motivá-los a continuar seus estudos.
        </p> */}
        <p>
          Além disso, a Formei! estabelece parcerias com instituições de ensino superior e empresas para facilitar o acesso dos estudantes a informações sobre cursos, bolsas de estudo e oportunidades de estágio. <br />
          <span className="text-primary"><b>Promovemos visitas a universidades e centros de pesquisa, a fim de familiarizar os alunos com o ambiente acadêmico e inspirá-los a buscar uma formação superior.</b></span>
        </p>
        {/* <p>
          Também oferecemos orientação vocacional e apoio na preparação para exames e vestibulares, auxiliando os estudantes no processo de escolha da carreira e na busca por oportunidades educacionais. <br/>Por meio de mentores e profissionais voluntários, fornecemos orientação personalizada, compartilhando experiências e conhecimentos valiosos.
        </p> */}
        <p>
          Acreditamos que todos os jovens têm o direito à educação e o potencial para alcançar seus sonhos acadêmicos. <br />
          Nossa ONG está comprometida em fornecer o suporte necessário para que eles possam trilhar o caminho da educação e conquistar um futuro promissor.
        </p>
        <p>
          Se você é um estudante do ensino fundamental ou médio em busca de orientação e motivação para continuar seus estudos, entre em contato conosco. </p>
        <p className="d-flex justify-content-center text-primary"><b>Juntos, podemos construir um futuro melhor através da educação.</b>
        </p>
      </div>
      <div className="text-center mb-4">
        <img src={formei} alt="Logo formei com a mensagem ''"/>
      </div>

    </div>
  );
}

export default Sobre;