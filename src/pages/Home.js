import React from "react"
import Card from './../components/Card'
//Import Images
import palestra from '../resources/img/publicacoes/palestra.png'
import laboratorio from '../resources/img/publicacoes/laboratorio.jpg'
import fazerDps from '../resources/img/publicacoes/fazerdepois.png'
import gastro from '../resources/img/publicacoes/gastro-1024x683.jpg'
import tipoEnsSup from '../resources/img/publicacoes/tipoEnsSup.png';
import faculdadeGratis from '../resources/img/publicacoes/faculdadeGratis.png';
import { Link } from "react-router-dom"

function Home() {
  return (
    <div className="page container-fluid">


      <div id="carouselExampleCaptions" className="carousel slide p-2">
        <div className="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img src={palestra} className="d-block w-100" alt="Sala de aula com palestrante" />
            <div className="carousel-caption d-block">
              <h5>Visita as Escolas</h5>
              <p>Um dos objetivos do projeto é visitar escolas para trazer informação aos alunos, estimulando para que continuem a jornada academia para além do colégio!
                <br/>Acompanhe nosso <Link className="fw-bold" to="agenda">calendário de visitas</Link>!
              </p>
            </div>
          </div>
          <div className="carousel-item">
            <img src={fazerDps} className="d-block w-100" alt="Imagem do Formei com frase 'O que fazer depois da escola'" />
            <div className="carousel-caption d-block">
              <h5>O que trazemos?</h5>
              <p>Aqui você pode encontrar textos completos, com links e explicações sobre educação - o que é o ensino superior, como acessar e quais as alternativas? Esses são alguns de nossos <Link className="fw-bold" to='posts'>conteúdos</Link> então fiquem ligados aqui para saber mais! 😉</p>
            </div>
          </div>
          <div className="carousel-item">
            <img src={gastro} className="d-block w-100" alt="Estudantes no labotratório de gastronomia no UniAcademia" />
            <div className="carousel-caption d-block">
              <h5>São muitas opções!</h5>
              <p className="text-justify">Atualmente temos diversas opções de cursos e áreas de carreira para seguir! Não estamos presos aos mesmos conhecimentos e podemos expandir nosso aprendizado! São muitas áreas, cursos e especializações - o que pode tornar a seleção um tanto complicada!
                Vale lembrar porém que sempre temos a possibilidade de trocar de curso ou área de atuação para outra que nos atende mais! Nunca se esqueça que você <b>pode</b> tentar novamente 😊</p>
            </div>
          </div>
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>

      <div className="news m-4">
        {/* Colocar apenas as 3 Publicacoes mais Recentes */}
        <h2 className="mt-2">Publicações recentes:</h2>
        <div className="row">


          <Card
            title="Posso fazer faculdade de graça?"
            text="Muitas pessoas sonham em fazer faculdade, mas desanimam só de pensar nos custos que vão ter que arcar. Mas não se preocupa que hoje vamos explorar algumas possibilidades que você pode explorar para tornar o sonho da graduação uma realidade e sem pesar no bolso!"
            img={faculdadeGratis} linkTo='/possofazerfaculdadedegraca' tags="#informacao #ensinoSuperior" />

          <Card
            title="Quais os tipos de ensino superior?"
            text="Sim, existe mais de um tipo de ensino superior! A maioria das pessoas conhece a famosa graduação, [...] mas também existem outras opções..., algumas ainda mais curtas [...]
              Enfim, vamos as opções 😉"
            img={tipoEnsSup} linkTo='/tiposDeEnsinoSuperior' tags="#informacao #ensinoSuperior" />


          <Card
            title="O que fazer depois do Ensino Médio?"
            text="É muito comum se perguntar sobre o que fazer após o ensino médio, já que um mundo novo se abre à frente dos estudantes. 
                    Mas é preciso manter a cabeça fria, pois é normal que apareçam dúvidas nessa fase em que as oportunidades são acompanhadas de incertezas e inseguranças. Mesmo no meio desse turbilhão de emoções, o momento requer que a pessoa tome decisões importantes, comprometendo-se a seguir uma carreira profissional. Em todo caso, é fundamental avaliar todas as opções com calma e consciência, para que a sua decisão seja a mais acertada possível."
            img={laboratorio} linkTo='/oquefazerdepois' tags="#informacao" />


        </div>

      </div>
    </div>

  );
}

export default Home;