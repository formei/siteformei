import ITA from '../../resources/img/publicacoes/ita-1024x768.jpg'
import baja from '../../resources/img/publicacoes/baja.jpg'
import anatomia from '../../resources/img/publicacoes/LaboratorioAnatomia-1024x518.jpg'
import culinaria from '../../resources/img/publicacoes/gastro-1024x683.jpg'
import ImageGallery from '../../components/ImageGallery';

function Oqueegraduacao() {

  const imgData = [
    {
      src: ITA,
      alt: "Projeto de AeroDesign (ITA)"
    },
    {
      src: anatomia,
      alt: "Laboratório de Anatomia"
    },
    {
      src: culinaria,
      alt: "Laboratório de Gastronomia"
    },
    {
      src: baja,
      alt: "Projeto Mini Baja"
    }
  ];


  return (
    <div className="single-post container">

      <h1 className="mt-2 mb-3">O que é graduação?</h1>
      <p>Na escola passamos por duas etapas de ensino: o fundamental e o médio.
      </p><p>
        Depois disso podemos ir para o ensino superior - a famosa <b className="text-primary">graduação</b>!
      </p><p>
        A graduação é uma etapa de ensino, que tem maior foco em uma área específica, e dá ao estudante um estudo mais qualificado sobre o assunto. Isso torna os profissionais aptos e mais preparados para exercer suas profissões.
      </p><p>
        Uma formação profissional, além de aumentar as chances de uma boa colocação no mercado de trabalho, dentro da legislação, aumenta também o nível de remuneração.
      </p><p>
        Muitas empresas (nacionais e internacionais) exigem o diploma no processo de contratação, que comprova a dedicação do profissional em se aprimorar na área, entendendo as novas tendências e desafios do mercado.
      </p><p>
        Além disso, devido a pluralidade no ensino, a visão de mundo dos estudantes se expande muito, permitindo que conheçam outras ideias, outras realidades e tenham novas experiências!
      </p><p>
        Na graduação há forte estimulo as atividades de pesquisa, extensão e ensino. Grupos de robótica, aeronáutica, automobilismo e empresas juniores são alguns exemplos dessas atividades. Além disso os alunos tem acesso a laboratórios onde podem utilizar na prática, aprendizados teóricos vistos em sala de aula!
      </p><p>

            <ImageGallery gallery = {imgData} galleryDescription="Possibilidades de projetos para alunos dentro da graduação" />

      </p><p>
        No Brasil, a graduação pode ser de 3 tipos: Bacharelado, Licenciatura e Tecnologia (não, não é uma graduação só pra mexer com computadores 😉)
      </p><p>
        Vamos mostrar a diferença entre eles mais pra frente!
      </p><p className="text-primary fw-bold fst-italic text-end text-wrap fs-2 mb-4 p-1">
        Acompanhe as nossas redes sociais pra não perder!
      </p>
      
    </div>
  );
}

export default Oqueegraduacao;