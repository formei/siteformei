
function Oquefazerdepois() {
  return (
    <div className="single-post container">

      <h1 className="text-align-center mt-3">O que fazer depois do Ensino Médio?</h1>

      <p>É muito comum se perguntar sobre o que fazer após o ensino médio, já que um mundo novo se abre à frente dos estudantes.
      </p>
      <p>
        Mas é preciso manter a cabeça fria, pois é normal que apareçam dúvidas nessa fase em que as oportunidades são acompanhadas de incertezas e inseguranças.<br />
        Mesmo no meio desse turbilhão de emoções, o momento requer que a pessoa tome decisões importantes, comprometendo-se a seguir uma carreira profissional. Em todo caso, é fundamental avaliar todas as opções com calma e consciência, para que a sua decisão seja a mais acertada possível.
      </p>
      <p>
        Pensando nisso, preparamos este conteúdo para mostrar quais são os melhores caminhos para quem concluiu ou está finalizando o ensino médio. As alternativas vão desde prestar o vestibular até procurar emprego, passando por prestar um concurso público, realizar um intercâmbio e buscar estágios
      </p>
      <h3>Quais são as opções após concluir o ensino médio?</h3>

      <h4 className="text-primary">Prestar vestibular</h4>
      <p>
        Uma opção muito escolhida após a conclusão do ensino médio é a de prestar vestibular para entrar em uma <a target="_blank" rel="noreferrer" href="https://www.escoladapaz.com.br/blog/faculdade-ou-curso-tecnico/">faculdade</a>. No entanto, isso deve ser bem planejado, já que é necessário decidir qual será o curso, entender como são as provas, estudar para obter uma boa nota etc. <br />
        É possível também fazer o Enem para tentar uma bolsa em instituições particulares.<br />
        A vantagem de prestar vestibular é o aumento das chances de conquistar as melhores vagas existentes no <a target="_blank" rel="noreferrer" href="https://www.escoladapaz.com.br/blog/mercado-de-trabalho-veja-como-se-manter-atualizado-na-area-de-saude">mercado de trabalho</a>. <br />
        Ao mesmo tempo, a opção implica um maior conhecimento sobre determinado ramo de atuação, contribuindo para o estudante conhecer novas pessoas, fazer contatos importantes e <a target="_blank" rel="noreferrer" href="https://www.escoladapaz.com.br/blog/como-escolher-carreira-profissional-de-acordo-personalidade">atuar com o que gosta de verdade</a>.
      </p>

      <h4 className="text-primary">Entrar em um curso técnico</h4>
      <p>
        Entrar em um curso técnico é uma alternativa para quem deseja ingressar rapidamente no mercado de trabalho e, o melhor, com uma capacitação. Em um tempo mais curto que o do ensino superior, é possível qualificar-se e, assim, candidatar-se a cargos interessantes e com bons salários.
        Nesse tipo de curso, o aluno vai aprender sobre a área escolhida de forma mais objetiva, por meio de aulas teóricas e práticas, com duração menor que em uma graduação, mas não menos difícil.<br />
        É importante ressaltar, ainda, a possibilidade de realizar o ensino técnico e o médio simultaneamente. No entanto, só é possível tornar-se técnico tendo o diploma de ensino médio em mãos.<br />
        De maneira geral, os cursos técnicos são muito bem-vistos no mercado de trabalho e possibilitam que o estudante decida se quer se aprofundar no ramo e, mais tarde, investir em uma graduação para se especializar em uma área do conhecimento humano.
      </p>


      <h4 className="text-primary">Procurar um estágios</h4>
      <p>Em geral, os <a target="_blank" rel="noreferrer" href="https://www.escoladapaz.com.br/blog/dicas-valiosas-para-um-bom-estagio-em-enfermagem">estágios</a> são uma consequência dos <a target="_blank" rel="noreferrer" href="https://www.escoladapaz.com.br/blog/conheca-principais-vantagens-dos-cursos-tecnicos">cursos técnicos</a>, profissionalizantes ou superiores. Contudo, existem muitos estudantes que buscam alguma vaga por conta própria.
        <br />Se você deseja trabalhar, procure empresas que contratam estagiários sem experiência, concedam bons conhecimentos da área e façam contrato com probabilidade de renovação, mesmo que em setores diferentes.
        Entre as vantagens que o estágio pode propiciar, estão:
        <ul>
          <li>enriquecimento do currículo devido às experiências adquiridas;</li>
          <li>e possibilidade de juntar dinheiro para o alcance de metas;</li>
        </ul>
        Além disso, o estágio é uma ótima oportunidade para conseguir vivência em um ambiente laboral, principalmente quando o candidato opta por um curso técnico, melhorando as chances de ser efetivado pela instituição.<br />
        <span>Importante destacar que empresas geralmente contratam estagiários que tenham alguma instituição de ensino vinculada, ou seja, que fazerm faculdade, curso técnico, ou mesmo ensino médio!</span>
      </p>

      <h4 className="text-primary">Inscrever-se no Programa Jovem Aprendiz</h4>
      <p>
        O Programa Jovem Aprendiz foi criado pelo governo federal para contribuir com a capacitação e a inserção dos jovens no mercado de trabalho. A iniciativa de responsabilidade social, que funciona no Brasil desde o início dos anos 2000, estimula a contratação de jovens sem experiência, garantindo a redução de custos trabalhistas nas empresas.<br />
        Desse modo, o aprendiz pode ser um adolescente ou jovem de 14 a 24 anos de idade. Mas para conseguir concorrer às vagas de emprego, é necessário estar matriculado e frequentar a escola, caso o candidato não tenha concluído o ensino médio no momento da inscrição.
        O programa é uma opção interessante para se qualificar para o mercado de trabalho, tanto para quem está cursando o ensino médio como para quem já concluiu essa etapa do desenvolvimento escolar.<br />
        O motivo é que a Lei do Aprendiz, a qual amplia o número de postos de trabalho para quem tem pouca experiência, desenvolve competências teóricas e práticas dos jovens, sendo fundamental na conquista do <a target="_blank" rel="noreferrer" href="https://www.escoladapaz.com.br/blog/como-conseguir-primeiro-emprego/">primeiro emprego</a>. Ao mesmo tempo, o jovem aprendiz tem direito:
        <ul>
          <li>
            Á carteira de trabalho assinada pelo empregador;
          </li>
          <li>
            Ao recolhimento do Fundo de Garantia por Tempo de Serviço (FGTS);
          </li>
          <li>
            A férias equivalentes ao período trabalhado;
          </li>
          <li>
            Ao 13º salário.
          </li>
        </ul>
      </p>

      <h4 className="text-primary">Procurar um emprego</h4>
      <p>Essa é uma solução na qual muitos jovens pensam inicialmente. A ideia é ter uma fonte de <a target="_blank" rel="noreferrer" href="https://www.escoladapaz.com.br/blog/conheca-opcoes-para-conquistar-uma-renda-extra-na-area-da-saude">renda própria</a>, para não depender tanto dos pais. No entanto, é necessário ter ponderação antes de sair distribuindo currículos.<br/>
        Como já foi dito, o objetivo principal é ter o próprio dinheiro. Mesmo não sendo o suficiente para ser totalmente independente, vai eliminar a necessidade de pedir mesada. Além disso, permite o desenvolvimento de responsabilidades, um melhor gerenciamento do dinheiro e, até mesmo, complementar o orçamento da casa.<br/>
        Contudo, existem algumas desvantagens, como trabalho sem formação específica, que <span className="fst-italic text-primary fw-semibold">pode comprometer a continuação dos estudos, o crescimento na carreira e demais perspectivas para o futuro</span>.
      </p>
    </div>
  );
}

export default Oquefazerdepois;