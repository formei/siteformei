import { Link } from 'react-router-dom';


function Possofazerfaculdadedegraca() {
    return (
      <div className="single-post container">
  
        <h1 className="text-align-center mt-3">Posso fazer faculdade de graça?</h1>
  
        <p>
        Muitas pessoas sonham em fazer faculdade, mas desanimam só de pensar nos custos que vão ter que arcar. 
        <br />Mas não se preocupa que hoje vamos explorar algumas possibilidades que você pode explorar para tornar o sonho da graduação uma realidade e sem pesar no bolso!
        </p>

        <h4 className="text-primary">Universidades Públicas</h4>
        <p>
        As universidades públicas brasileiras são instituições de ensino mantidas pelo governo federal ou pelos governos estaduais, oferecendo diversos cursos de graduação e pós-graduação em diversas áreas do conhecimento. 
        Você pode ingressar nessas instituições por meio de processos seletivos, como vestibulares específicos de cada universidade ou o Exame Nacional do Ensino Médio (ENEM). 
        <br />Uma vez aprovados, os estudantes não precisam pagar mensalidades, pois o ensino é gratuito. 
        No entanto, ainda podem haver custos indiretos, como a compra de livros e materiais didáticos.
        </p>

        <h4 className="text-primary">Instituições Federais</h4>
        <p>
        Além das universidades o sistema federal também possui os Institutos Federais! São instituições de ensino pluricurriculares e multicampi - ou seja, oferecem vários tipos de educação profissional, desde o nível técnico a graduação e pós-graduação, incluindo tipos diferentes como Licenciatura e Bacharelado. Além disso, sendo multicampi estão presentes em diversas regiões do país.<br/>
        Para mais detalhes sobre todos esses tipos de curso, veja nossa publicação "<Link to='tiposDeEnsinoSuperior'>Quais os tipos de ensino superior?</Link>".
        </p>

        <h4 className="text-primary">Programa Universidade para Todos (ProUni)</h4>
        <p>
        O ProUni é um programa do governo federal criado para facilitar o acesso de estudantes de baixa renda à educação superior em instituições privadas. 
        O programa oferece bolsas de estudo parciais (50%) ou integrais (100%) para cursos de graduação. 
        <br />Para entrar no programa você deve ter realizado o ENEM e atender a critérios de renda familiar estabelecidos. 
        A seleção é baseada na pontuação do ENEM, sendo priorizados os estudantes com melhores desempenhos. 
        <br />As bolsas do ProUni cobrem as mensalidades do curso, permitindo que os beneficiados estudem sem pagar a parte correspondente ao valor da bolsa.
        </p>

        <h4 className="text-primary">Bolsas e auxílios</h4>
        <p>
        algumas universidades e instituições privadas de ensino superior oferecem programas de bolsas de estudo e auxílios financeiros. 
        Essas bolsas podem ser baseadas em mérito acadêmico, necessidade financeira ou critérios específicos determinados pela instituição. 
        <br />Alguns exemplos são bolsas de estudo por desempenho acadêmico excepcional, bolsas de pesquisa, bolsas para estudantes com deficiência, bolsas para estudantes de minorias étnicas e sociais, entre outras. 
        Essas bolsas e auxílios podem variar em termos de cobertura financeira, desde descontos parciais nas mensalidades até bolsas integrais que cobrem todos os custos da faculdade.
        </p>
        <p className="mb-4">
        <span className="text-primary fw-bold mt-3">Então, sim! É possível fazer faculdade de graça</span>, com ajuda de programas do governo, universidades públicas e bolsas você consegue explorar várias opções sem precisar quebrar o porquinho.
        <br />Lembre-se sempre de pesquisar sobre suas possibilidades, buscando ver os benefícios e se você atende os requisitos.
        </p>
      </div>
    );
  }
  
  export default Possofazerfaculdadedegraca;