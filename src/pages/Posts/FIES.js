function FIES() {
    return (
        <div className="single-post container">
            <h1 className="mt-2 mb-3">O que é e como usar o FIES?</h1>
            Preparados para o vestibular e a próxima etapa da vida acadêmica? Hoje vamos falar sobre o FIES, o Fundo de Financiamento Estudantil,  
            um programa que pode ajudar muito na realização do sonho de fazer uma faculdade, sendo um recurso valioso para 
            quem quer entrar na faculdade, mas não tem condições financeiras de arcar com os custos imediatamente. 
            <br />
            <p>
            Esse é um programa do governo federal que oferece financiamento para estudantes pagarem a faculdade. 
            Com o FIES, você pode começar a estudar agora e pagar as mensalidades depois de formado, de forma parcelada.
            </p>
            <h4 className="text-primary">Vantagens do FIES</h4>
            <p>
            Facilidade no pagamento: Uma das maiores vantagens do FIES é a facilidade no pagamento. 
            Diferente de outros tipos de financiamento, no FIES você só começa a pagar a dívida depois de se formar e começar a trabalhar. 
            Isso significa que você pode se dedicar integralmente aos estudos sem a preocupação imediata de como pagar as mensalidades. 
            Após a formatura, você terá um período de carência antes de começar a pagar, o que ajuda a organizar suas finanças e se estabilizar no mercado de trabalho.
            </p>
            <p>
            Juros baixos: Os juros do FIES são significativamente mais baixos em comparação a outros tipos de empréstimos estudantis ou financiamentos bancários. 
            Isso torna o pagamento da dívida mais acessível e menos oneroso a longo prazo. Os juros reduzidos ajudam a minimizar o impacto financeiro e 
            garantem que você não vai pagar muito mais do que o valor original das mensalidades ao longo dos anos. 
            Com taxas mais baixas, o FIES se torna uma opção financeiramente vantajosa para quem precisa de apoio para cursar o ensino superior.
            </p>
            Acesso à educação:
            O FIES desempenha um papel crucial no acesso à educação superior para estudantes de baixa renda. 
            Ao proporcionar financiamento acessível, ele permite que mais estudantes realizem o sonho de entrar na faculdade, 
            independentemente da sua situação financeira. Esse acesso ampliado contribui para a inclusão social e democratização do ensino superior, 
            permitindo que talentos e potencialidades de todas as classes sociais sejam desenvolvidos. Com o FIES, 
            muitos estudantes que, de outra forma, não poderiam arcar com os custos da faculdade, têm a oportunidade de conquistar um diploma e construir um futuro melhor.
            <br />
            <br />
            <h4 className="text-primary">Como se cadastrar?</h4>
            - Tenha uma nota no Enem: Você precisa ter feito o Enem a partir de 2010, ter obtido no mínimo 450 pontos nas provas e não ter zerado a redação.
            <br />
            - Cadastro no site: Acesse o site do FIES (fies.mec.gov.br) e faça sua inscrição dentro do prazo estipulado.
            <br />
            - Preencha os dados: Insira seus dados pessoais, escolha o curso e a instituição de ensino desejada.
            <br />
            - Aguarde o resultado: Se aprovado, você deverá confirmar suas informações na Comissão Permanente de Supervisão e Acompanhamento (CPSA) da instituição escolhida.
            <br />
            - Formalização do financiamento: Com a documentação aprovada, vá ao banco (Caixa Econômica Federal ou Banco do Brasil) para formalizar o contrato. Será necessário ter um fiador.
            <br />
            <br />
            <h4 className="text-primary">Vale a pena?</h4>
            <p> 
            Se você deseja entrar na faculdade e não tem condições de pagar as mensalidades agora, o FIES pode ser uma excelente opção. 
            Ele permite que você foque nos estudos e só comece a pagar depois de se formar e estar empregado. 
            <br />
            <br />
            Agora que você já conhece o FIES, pesquise as instituições e cursos de seu interesse. Boa sorte na sua jornada acadêmica e rumo à realização dos seus sonhos!
            </p>
            
            <br />
            <br />
        </div>
    );
}
export default FIES;