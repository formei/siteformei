function PISM() {
    return (
        <div className="single-post container">
            <h1 className="mt-2 mb-3">PISM - Um vestibular dividido em 3 anos!</h1>
            Se você está se preparando para ingressar em uma universidade e busca uma alternativa que permita uma preparação 
            gradual e mais aprofundada, o PISM (Programa de Ingresso Seletivo Misto) pode ser a resposta que você procura. Esse vestibular, adotado por algumas 
            universidades brasileiras, divide a avaliação em três etapas ao longo do ensino médio, oferecendo uma abordagem única e abrangente.

            O PISM é um sistema de avaliação adotado pela Universidade Federal de Juiz de Fora (UFJF), 
            que propõe uma forma diferenciada de seleção de estudantes. Ao invés de concentrar todo o conteúdo em uma única prova, o PISM divide a avaliação 
            em três etapas correspondentes a cada ano do ensino médio.
            <br />
            <br />
            <h4 className="text-primary">O Processo ao Longo dos Anos</h4>
            <p>
            - 1. Primeiro Ano:
            No primeiro ano do ensino médio, os estudantes enfrentam a primeira etapa do PISM, que aborda conhecimentos gerais e básicos. Essa abordagem inicial permite 
            uma adaptação gradual ao estilo das questões, além de fornecer uma base sólida para os anos seguintes.
            </p>
            <p>
            - 2. Segundo Ano:
            Com o avanço para o segundo ano, a segunda etapa do PISM torna-se mais desafiadora, incorporando conteúdos mais específicos e aprofundados. Essa fase visa 
            avaliar o desenvolvimento contínuo dos estudantes e prepará-los para a complexidade das questões que encontrarão no vestibular.
            </p>
            - 3. Terceiro Ano:
            O terceiro ano é o ápice do processo, com a última etapa do PISM concentrando-se em temas complexos e detalhados. Essa fase final visa não apenas avaliar o 
            conhecimento adquirido ao longo dos anos, mas também preparar os estudantes para os desafios acadêmicos que enfrentarão na universidade.
            <br />
            <br />
            <h4 className="text-primary">Vantagens do Pism</h4>
            No PISM, a divisão em três anos permite uma preparação mais gradual e menos estressante, o que evitando a sobrecarga de informações em um curto período de tempo. 
            Isso também permite uma avaliação contínua do desempenho dos estudantes ao longo do ensino médio, permitindo ajustes e melhorias constantes além de
            ajudar no desenvolvimento progressivo das habilidades cognitivas e analíticas dos estudantes, os preparando não apenas para o vestibular, mas também 
            para os desafios acadêmicos futuros.
            <br />
            <br />
            <h4 className="text-primary">Dicas para o Sucesso no PISM</h4>
            <p> - Elabore um plano de estudos que leve em consideração a progressão das etapas do PISM e distribua seu tempo de forma equilibrada ao longo dos três anos. </p>
            <p> - Se lembre de manter revisões constantes dos conteúdos aprendidos, que garanta uma assimilação mais efetiva e duradoura.</p>
             - Realize simulações de provas para se familiarizar com o estilo das questões e a dinâmica do vestibular.
            <br />
            <br />
            O PISM oferece uma abordagem inovadora e eficiente para o processo seletivo universitário. Ao distribuir a avaliação ao longo de três anos, 
            proporciona uma preparação mais consistente e um desenvolvimento gradual das habilidades necessárias para o sucesso acadêmico. Se você está prestes 
            a iniciar sua jornada rumo à universidade, considere o PISM como uma opção que pode fazer toda a diferença em sua trajetória educacional.
            <br />
            <br />
        </div>
    )
}
export default PISM;