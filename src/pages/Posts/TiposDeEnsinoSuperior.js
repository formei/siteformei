
function TiposDeEnsinoSuperior() {
    return (
        <div className="single-post container">
            <h1 className="mt-2 mb-3">Quais os tipos de ensino superior?</h1>
            <p>
                Sim, existe mais de um tipo de ensino superior! A maioria das pessoas conhece a famosa <span className="text-primary fw-bold">graduação</span>, conquista através de faculdades e universidades!<br />
                Porém também existem outras opções de ensino superior, algumas ainda mais curtas do que a graduação, permitindo ingresso mais rápido no mercado de trabalho!<br />
                Enfim, vamos as opções 😉
            </p>
            <h4 className="text-primary">Cursos Sequenciais</h4>
            <p>
                Podem ser de <span className="fw-bold">complementação de estudos</span> ou de <span className="fw-bold">formação específica</span>! <br />
                Para alguém que acaba de sair do Ensino Médio o <span className="fw-bold text-primary">curso de complementação de estudos</span> é o mais indicado, porque não exige que o aluno esteja cursando graduação na área.<br />
                Basicamente são cursos sobre assuntos específicos, como <i>Panificação</i>, <i>Designer</i>, <i>Fotografia</i>, entre outras. Muitas instituições oferecem esses cursos, principalmente no formato de ensino a distância (EAD).<br />
                Existem também cursos de extensão, que <b>não são considerados como ensino superior</b>, mas oferecem conhecimentos em área específica desejada pelo aluno. Um exemplo são os cursos de extensão ofertados pelo <a href="https://www.uniacademia.edu.br/cursos-extensao" rel="noreferrer" target="_blank">UniAcademia</a>
            </p>
            <h4 className="text-primary">Tecnólogo</h4>
            <p>
                O Tecnólogo também pode ser conhecido como curso de tecnologia, e costuma ter duração de 2 a 3 anos.<br />
                São menos matérias teóricas e mais atividades voltadas ao mercado de trabalho na prática!
                <br />
                O Tecnólogo exige a conclusão do ensino médio, mas oferta um diploma ao final, é reconhecido como Ensino Superior e ainda permite que o aluno siga a carreira nos estudos, sendo aceito para ingresso em qualquer tipo de pós-graduação.<br />
                Importante destacar que o tecnólogo visa o ensino mais específico de uma área, então se você anda não sabe muito bem onde atuar, talvez a graduação seja a mais recomendada no momento.<br />
                Alguns exemplos de cursos são <a href="https://www.uniacademia.edu.br/curso/gestao-de-recursos-humanos" rel="noreferrer" target="_blank">Gestão de Recursos Humanos</a>, <a href="https://www.uniacademia.edu.br/curso/marketing" rel="noreferrer" target="_blank">Marketing</a>, <a href="https://www.uniacademia.edu.br/curso/gastronomia" rel="noreferrer" target="_blank">Gastronomia</a>, e muitos outros!
            </p>
            <h4 className="text-primary">Licenciatura</h4>
            <p>
                A Licenciatura <span className="text-primary">é um tipo de graduação</span> com foco em formar pessoas para a área de <span className="text-primary fw-bold">docência</span>, ou seja, professores do ensino fundamental e ensino médio!<br />
                Além dos conteúdos voltados a área de estudo da graduação, os alunos também aprendem sobre pedagogia, técnicas de didática, teorias de aprendizagem, entre outros temas. O objetivo é que o aluno adquira conhecimentos para que possa ensinar aquilo que aprendeu da melhor forma possível.<br />
                Os cursos podem durar de 3 a 5 anos, como por exemplo cursos de <a href="https://www2.ufjf.br/ufjf/ensino/graduacao/artes-visuais-licenciatura/" rel="noreferrer" target="_blank">Artes Visuais</a>, <a href="https://www2.ufjf.br/ufjf/ensino/graduacao/letras/" rel="noreferrer" target="_blank">Letras – Tradução</a> e <a href="https://www2.ufjf.br/ufjf/ensino/graduacao/matematica/" rel="noreferrer" target="_blank">Matemática</a>.
            </p>

            <h4 className="text-primary">Bacharelado</h4>
            <p>
                É um <span className="text-primary">é um tipo de graduação</span> essencial para exercer algumas profissões, como advocacia, medicina, arquitetura e engenharia.<br />
                Dura em média de 3 a 6 anos, e o objetivo é fornecer ao estudante conhecimentos gerais sobre a área em estudo, dando uma base sólida nos fundamentos. Dessa forma <span className="text-primary">o aluno adquire uma gama de conhecimentos em sua área e pode escolher depois em qual deseja atuar no mercado</span>.<br />
                Em cursos de bacharelado o aluno geralmente deve realizar o estágio obrigatório (uma oportunidade de já entrar no mercado de trabalho visando o aprendizado) e devem realizar um Trabalho de Conclusão de Curso (TCC).<br />
                Destaca-se que, <i>em algumas profissões, além do diploma, é preciso obter um registro de órgãos regulamentadores da profissão</i>.
                <br />Alguns exemplos são:
            </p>
            <ul className="ms-2">
                <li>
                    Formados em Direito devem ser aprovados no exame da Ordem dos Advogados do Brasil (OAB);
                </li>
                <li>
                    Formados em Engenharia, muitas das vezes, necessitam se registrar no Conselho Regional de Arquitetura e Agronomia (CREA) para exercer a profissão;
                </li>
                <li>
                    Formados em medicina necessitam ter a liberação do Conselho Regional de Medicina (CRM);
                </li>
            </ul>
            {/* Refrencias:
            - https://www.alura.com.br/artigos/ensino-superior#tipos-de-ensino-superior:-bacharelado-licenciatura-tecnologo-e-sequencial
            - https://www.universia.net/br/actualidad/orientacion-academica/os-tipos-cursos-do-ensino-superior-brasil-1149602.html
            - https://www.guiadacarreira.com.br/blog/licenciatura
            */}
            <h5 className="text-primary mt-4">Aqui temos algumas faculdades e universidades para procurar cursos que combinam com você!</h5>
            <div className="rounded list-group list-group-flush mt-4 mb-4">

                <a className="list-group-item list-group-item-action list-group-item-primary" href="https://www.uniacademia.edu.br/cursos" rel="noreferrer" target="_blank">UniAcademia</a>

                <a className="list-group-item list-group-item-action list-group-item-primary" href="https://www2.ufjf.br/ufjf/ensino/" rel="noreferrer" target="_blank">UFJF</a>

                <a className="list-group-item list-group-item-action list-group-item-primary" href="https://www.ifsudestemg.edu.br/cursos/" rel="noreferrer" target="_blank">IF Sudeste MG</a>

            </div>
        </div>
    )
}
export default TiposDeEnsinoSuperior;