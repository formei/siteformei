import Card from './../components/Card'
//Import Images
import acabeiEnsMed from '../resources/img/publicacoes/acabeiEMedio.jpg';
import laboratorio from '../resources/img/publicacoes/laboratorio.jpg';
import faculdadeGratis from '../resources/img/publicacoes/faculdadeGratis.png';
import tipoEnsSup from '../resources/img/publicacoes/tipoEnsSup.png';
import cursoEscolha from '../resources/img/publicacoes/cursoEscolha.png';

function Posts() {
    return (
        <div className="container mt-2" id="posts-page">
            <h1 className="mb-4">Publicações</h1>

            <div className="row mb-4">

                <Card
                    title="Posso fazer faculdade de graça?"
                    text="Muitas pessoas sonham em fazer faculdade, mas desanimam só de pensar nos custos que vão ter que arcar. Mas não se preocupa que hoje vamos explorar algumas possibilidades que você pode explorar para tornar o sonho da graduação uma realidade e sem pesar no bolso!"
                    img={faculdadeGratis} linkTo='/possofazerfaculdadedegraca' tags="#informacao #ensinoSuperior" />

                <Card
                    title="Quais os tipos de ensino superior?"
                    text="Sim, existe mais de um tipo de ensino superior! A maioria das pessoas conhece a famosa graduação, [...] mas também existem outras opções..., algumas ainda mais curtas [...] Enfim, vamos as opções 😉"
                    img={tipoEnsSup} linkTo='/tiposDeEnsinoSuperior' tags="#informacao #ensinoSuperior" />

                <Card
                    title="O que fazer depois do Ensino Médio?"
                    text="É muito comum se perguntar sobre o que fazer após o ensino médio, já que um mundo novo se abre à frente dos estudantes. 
                    Mas é preciso manter a cabeça fria, pois é normal que apareçam dúvidas nessa fase em que as oportunidades são acompanhadas de incertezas e inseguranças. Mesmo no meio desse turbilhão de emoções, o momento requer que a pessoa tome decisões importantes, comprometendo-se a seguir uma carreira profissional. Em todo caso, é fundamental avaliar todas as opções com calma e consciência, para que a sua decisão seja a mais acertada possível."
                    img={laboratorio} linkTo='/oquefazerdepois' tags="#informacao #ensimomedio" />

                <Card
                    title="O que é graduação?"
                    text="Na escola passamos por duas etapas de ensino: o fundamental e o médio [...] Pensando nisso, preparamos este conteúdo para mostrar quais são os melhores caminhos para quem concluiu ou está finalizando o ensino médio."
                    img={acabeiEnsMed} linkTo='/oqueegraduacao' tags="#informacao #graduacao" />
                <Card
                    title="Qual curso fazer?"
                    text="Escolher um curso após o ensino médio é uma decisão importante que pode moldar o seu futuro. [...] Neste artigo buscaremos te ajudar e daremos dicas para facilitar essa escolha tão importante."
                    img={cursoEscolha} linkTo='/qualcursofazer' tags="#informacao #graduacao" />
                <Card
                    title="PISM - Um vestibular dividido em 3 anos!"
                    text="Se você está se preparando para ingressar em uma universidade [...] o PISM (Programa de Ingresso Seletivo Misto) pode ser a resposta que você procura."
                    img={acabeiEnsMed} linkTo='/PISM' tags="#informacao #graduacao" />
                <Card
                    title="O que é e como usar o FIES?"
                    text="Hoje vamos falar sobre o FIES, o Fundo de Financiamento Estudantil [...] um recurso valioso para 
                    quem quer entrar na faculdade, mas não tem condições financeiras de arcar com os custos imediatamente."
                    img={acabeiEnsMed} linkTo='/FIES' tags="#informacao #graduacao" />
            </div>
        </div>
    )
}

export default Posts;