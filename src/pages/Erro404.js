import React from "react"
import { Link } from "react-router-dom";

function Erro404() {

  return (
        <div className="container mt-4">
          <h3 className="text-danger">A página que você buscou não foi encontrada.</h3>
          <Link className="mt-4 btn btn-outline-primary" type="button" to='/'>Voltar</Link>
        </div>
  );
}

export default Erro404;