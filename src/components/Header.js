import React from "react"
import LOGO from '../resources/img/logo.jpg'
import { Link } from 'react-router-dom';

function Header() {

  function closeBurguerMenu() {
    let nav = document.getElementsByClassName("navbar-collapse");
    nav[0].classList.add('hideElement');    
    nav[0].classList.remove("show");
  }

  function openBurguerMenu() {
    let nav = document.getElementsByClassName("navbar-collapse");
    nav[0].classList.remove("hideElement");
  }

  return (

    <nav className="navbar navbar-expand-lg">
      <div className="container-fluid ">
        <Link to='/' className="logo"><img src={LOGO} alt="Formei project Logo" /></Link>

        <button onClick={openBurguerMenu} className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>


        <div className="collapse navbar-collapse justify-content-end" id="navbarTogglerDemo01">
          <ul className="navbar-nav mb-2 mb-lg-0 mr-0 text-end">
            <li className="nav-item">
              <Link to="/" className="nav-link active text-light" aria-current="page" onClick={closeBurguerMenu}>Home</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link active text-light" onClick={closeBurguerMenu} to="/sobre">Sobre</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link active text-light" onClick={closeBurguerMenu} to="/posts">Posts</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link active text-light" onClick={closeBurguerMenu} to="/agenda">Visitas</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link active text-light" onClick={closeBurguerMenu} to="/contato">Contato</Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>


  );
}

export default Header;