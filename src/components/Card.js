import { Link } from 'react-router-dom';

function Card({ title, text, img, linkTo, tags }) {
    return (

        <div className="col-sm-4 mb-3 mb-sm-0 mt-2">
            <div className="card">
                <div className="card-image-header">
                    <img src={img} className="card-img-top" alt="Imagem representando o post" />
                    <h5 className="card-title p-2 mt-1">{title}</h5>
                </div>
                <div className="card-body">
                    <p className="card-text">{text}</p>
                    <div className="d-flex justify-content-between">
                        <Link to={linkTo} className="btn btn-outline-primary">Ler mais</Link>
                        <span className="text-body-secondary">{tags}</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Card;