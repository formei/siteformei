
function ImageGallery(props) {

    return (

        <div className="d-flex flex-column justify-content-center">
            <div className="d-flex justify-content-center">
                <div className="row w-50">
                    {props.gallery.map((item, index) => (
                        <div className="col-md-6 mb-3">
                            <img
                                src={item.src}
                                className="w-100 shadow-1-strong rounded"
                                alt={item.alt}
                            />
                        </div>
                    ))
                    }
                </div>
            </div>
            <p className="text-center text-muted fw-light">{props.galleryDescription}</p>
        </div>

    );
}

export default ImageGallery;