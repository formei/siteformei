import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
/*resources*/
import './resources/bootstrap/css/bootstrap.css';
import './resources/bootstrap/css/bootstrap.min.css';
import './resources/formei.css';
import './resources/animations.css';
import './resources/calendar.css';
/* components */
import Header from './components/Header';
import Footer from './components/Footer';
/* pages */
import Home from './pages/Home';
import Sobre from './pages/Sobre';
import Contato from './pages/Contato';
import Posts from './pages/Posts';
import Erro404 from './pages/Erro404';
import Agenda from './pages/Agenda';

/* posts */
import OqueGraduacao from './pages/Posts/Oqueegraduacao';
import OqueFazerDepois from './pages/Posts/Oquefazerdepois';
import TiposDeEnsinoSuperior from './pages/Posts/TiposDeEnsinoSuperior';
import Opcoesdevestibular from './pages/Posts/Opcoesdevestibular';
import Possofazerfaculdadedegraca from './pages/Posts/Possofazerfaculdadedegraca';
import Escolherseucurso from './pages/Posts/Comoeescolherseucurso';
import FIES from './pages/Posts/FIES';
import PISM from './pages/Posts/PISM';

function App() {
  return (
    <div>

      <Header />

      <Switch>
        <Route path='/' component={Home} exact />
        <Route path='/sobre' component={Sobre} />
        <Route path='/contato' component={Contato} />
        <Route path='/posts' component={Posts} />
        <Route path='/agenda' component={Agenda} />

        {/* Paginas dos posts */}
        <Route path='/oqueegraduacao' component={OqueGraduacao} />
        <Route path='/oquefazerdepois' component={OqueFazerDepois} />
        <Route path='/tiposDeEnsinoSuperior' component={TiposDeEnsinoSuperior} />
        <Route path='/opcoesdevestibular' component={Opcoesdevestibular} />
        <Route path='/possofazerfaculdadedegraca' component={Possofazerfaculdadedegraca} />
        <Route path='/qualcursofazer' component={Escolherseucurso} />
        <Route path='/PISM' component={PISM} />
        <Route path='/FIES' component={FIES} />

        {/* Parte do Erro 404 */}
        <Route path="/erro404" component={Erro404} />
        <Redirect to="/erro404" />
        {/* Fim da parte do Erro 404 */}

      </Switch>

      <Footer />

    </div>

  );
}


export default App;
