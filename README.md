# Site Formei

Projeto de Extensão do [UniAcademia](https://www.uniacademia.edu.br/).  

Formei é um projeto voltado a trazer incentivo e informação a alunos de ensino fundamental e em especial ensino médio sobre formas de ingressar em um curso universitário, divulgando em redes sociais, cartazes e realizando palestras em escolas públicas.

## Sobre o site

**Hospedagem**: GitLab  
<!-- **CI/CD**: GitLab   -->
**Deploy**: Netlify

Acesso: [https://formei.netlify.app/](https://formei.netlify.app/)

**To Do list:**

- [X] Home
- [X] Sobre
- [X] Contatos
- [X] Calendário de visitas
- [X] Página de posts
- [X] Página individual de Post
  - [X] *Post* - O que fazer depois do Ensino Médio
  - [X] *Post* - O que é Graduação?
  - [X] *Post* - Quais os tipos de ensino superior?
  - [X] *Post* - Qual curso fazer?
  - [X] *Post* - PISM - Um vestibular dividido em 3 anos!
  - [X] *Post* - Posso fazer faculdade de graça?
  - [ ] *Post* - Opções de Vestibular
  - [ ] *Post* - O que é e Como usar o FIES
  - [ ] *Post* - O que é e Como usar o PROUNI
- [X] Configurar Deploy (Netlify) <!-- Configurar CI/CD - [Como configurar Gitlab CI e React em poucos passos](https://medium.com/qualyteam-engineering/como-configurar-gitlab-ci-e-react-em-poucos-passos-dd5e032b2a49) -->
- [X] Aplicar animações ao CSS
- [ ] Criar testes unitários
- [ ] Criar testes de interface


Estrutura de pastas:  
```
|- public
|- src
    |- components
        |- Card
        |- Footer
        |- Header
    |- pages
        |- Posts
            |- O que é graduação
            |- O que fazer depois do ensino médio            
        |- Contato
        |- Erro404
        |- Home
        |- Posts
        |- Sobre
    |- resources
        |- bootstrap
            |- ...
        |- img
            |- ...
        |- formei.css
        |- animations.css
        |- index.css
    |- App.js
    |- index.js
|- .gitignore
|- package.json

```
